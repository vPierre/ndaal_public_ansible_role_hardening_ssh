.
├── CODE_OF_CONDUCT.md
├── CODE_OF_CONDUCT.txt
├── CODE_OF_CONDUCT_DE.md
├── CODE_OF_CONDUCT_DE.txt
├── README.md
├── SECURITY.md
├── SECURITY.rst
├── Vulnerability_Disclosure_Policy.md
├── add_directory_documentation.py
├── create_robots.sh
├── dataset
│   └── placeholder.txt
├── dep
│   └── placeholder.txt
├── documentation
│   ├── robots.txt
│   └── scorecard
│       └── scorecard.md
├── example
│   └── sshd_config
├── humans.txt
├── process_scorecard_results.py
├── repo_setup.sh
├── res
│   └── placeholder.txt
├── robots.txt
├── security.txt
├── src
│   └── placeholder.txt
├── structure.txt
├── test
│   └── placeholder.txt
└── tools
    ├── SSH_hardening
    │   ├── README.md
    │   ├── defaults
    │   │   └── main.yml
    │   ├── files
    │   │   ├── ssh_password
    │   │   └── sshd
    │   ├── handlers
    │   │   └── main.yml
    │   ├── molecule
    │   │   └── default
    │   │       ├── converge.yml
    │   │       ├── molecule.yml
    │   │       ├── prepare.yml
    │   │       └── verify.yml
    │   ├── tasks
    │   │   ├── ca_keys_and_principals.yml
    │   │   ├── crypto_ciphers.yml
    │   │   ├── crypto_hostkeys.yml
    │   │   ├── crypto_kex.yml
    │   │   ├── crypto_macs.yml
    │   │   ├── hardening.yml
    │   │   ├── main.yml
    │   │   └── selinux.yml
    │   ├── templates
    │   │   ├── authorized_principals.j2
    │   │   ├── openssh.conf.j2
    │   │   ├── opensshd.conf.j2
    │   │   ├── revoked_keys.j2
    │   │   └── trusted_user_ca_keys.j2
    │   └── vars
    │       ├── Amazon_2.yml
    │       ├── Archlinux.yml
    │       ├── Debian.yml
    │       ├── Fedora.yml
    │       ├── Fedora_37.yml
    │       ├── FreeBSD.yml
    │       ├── OpenBSD.yml
    │       ├── RedHat.yml
    │       ├── RedHat_7.yml
    │       ├── RedHat_9.yml
    │       ├── SmartOS.yml
    │       ├── Suse.yml
    │       └── main.yml
    └── placeholder.txt

19 directories, 60 files
